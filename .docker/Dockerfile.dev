FROM gcr.io/omnisend-ops/golang-ci
# https://bitbucket.org/soundest/golang-ci/src/master/Dockerfile

RUN apk add --no-cache \
        autoconf \
        automake \
        libtool \
        gettext \
        gettext-dev \
        make \
        g++ \
        texinfo \
        vim \
        nano \
        nginx \
        socat \
        supervisor \
        # reports_automation
        gnumeric \
        \
        && rm -rf /var/cache/apk/*

RUN addgroup -g 1000 omnisend && adduser -u 1000 -G omnisend -H -D omnisend

RUN go get -u \
        github.com/githubnemo/CompileDaemon \
        github.com/go-delve/delve/cmd/dlv \
        github.com/swaggo/swag/cmd/swag

RUN git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"

RUN mkdir /root/.ssh && chmod 0700 /root/.ssh && \
        ssh-keyscan -t rsa bitbucket.org >> /root/.ssh/known_hosts && \
        ssh-keyscan -t rsa github.com >> /root/.ssh/known_hosts

# Supervisord configuration
RUN echo -e "[supervisord]\nnodaemon=false\nuser=root\nlogfile=/dev/null\npidfile=/etc/supervisord.pid\n\n[program:nginx]\ncommand=nginx -g 'daemon off;'\n\n[program:socat]\ncommand=socat TCP-LISTEN:4150,fork,reuseaddr TCP:dev-tunnel:4150" > /etc/supervisord.conf

# Nginx Reverse proxy for NSQ Node
RUN mkdir -p /run/nginx && echo "server { listen 4151; server_name development-service-nsq-n1-us-central1-a; location / { proxy_pass http://dev-tunnel:4151; }}" > /etc/nginx/conf.d/nsqd-http-proxy.conf

WORKDIR /go/src/bitbucket.org/soundest/go-scripts
