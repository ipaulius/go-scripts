package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"
)

func main() {
	fmt.Println("Hello brands :)")

	reader := NewBrandsReader("mongodb://contacts_segmentation:contacts_segmentation@localhost:27017")
	defer reader.Close()

	var apiAccessBrandIds = make(map[string]int, 1000)
	/*
	brandIdsChan := reader.ReadApiAccessibleBrandIds()
	for brandId := range brandIdsChan {
		apiAccessBrandIds[*brandId] = 1
	}
	*/

	inactivityPlan := []int { 30, 45, 60, 75, 90, 180, 360, 1000 }
	results := run(reader, inactivityPlan, apiAccessBrandIds)

	fmt.Printf("%5s, %10s, %10s\n", "days", "brands", "contacts")
	for _, r := range results {

		fmt.Printf("%5d, %10d, %10d\n", r.inactivityDays, r.brandCount, r.contactCount)
	}
}

func run(reader *brandsReader, inactivityPlan []int, apiAccessBrandIds map[string]int) []aggregatedBrandStats {
	now := time.Now()

	results := make([]aggregatedBrandStats, 0, len(inactivityPlan))
	for _, inactivityDays := range inactivityPlan {
		agg := aggregatedBrandStats{}
		agg.inactivityDays = inactivityDays

		brandChan := reader.ReadEligibleBrands(now, inactivityDays)

		for b := range brandChan {
			// fake add (temporary until api access mongo read rights are not available):
			apiAccessBrandIds[b.ID] = 1

			if _, exist := apiAccessBrandIds[b.ID]; exist {
				//fmt.Printf("E: %s; %s; %d\n", b.ID, b.Name, b.Data.Contacts.TotalCount)
				agg.aggregate(b)
			} else {
				//fmt.Printf("NE: %s; %s; %d\n", b.ID, b.Name, b.Data.Contacts.TotalCount)
			}
		}

		results = append(results, agg)
	}

	return results
}

type aggregatedBrandStats struct {
	inactivityDays int
	brandCount int
	contactCount int
}

func (agg *aggregatedBrandStats) aggregate(brand *Brand) {
	agg.brandCount++
	agg.contactCount += brand.Data.Contacts.TotalCount
}

type brandsReader struct {
	mongoClient *mongo.Client
}

func NewBrandsReader(connUri string) *brandsReader {
	return &brandsReader{
		mongoClient: initClient(connUri),
	}
}

func initClient(connUri string) *mongo.Client {
	// Connect to MongoDB
	ctx := context.Background()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connUri))

	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")

	return client
}

func (r *brandsReader) Close() {
	if r.mongoClient != nil {
		r.mongoClient.Disconnect(nil)
	}
}

type Brand struct {
	ID   string `bson:"_id,omitempty"`
	Name string `bson:"name,omitempty"`
	Data BrandData `bson:"data,omitempty"`
}

type BrandData struct {
	Contacts BrandDataContacts `bson:"allContacts,omitempty"`
}

type BrandDataContacts struct {
	TotalCount int	`bson:"totalCount,omitempty"`
}

func (r *brandsReader) ReadEligibleBrands(now time.Time, inactivityDays int) <-chan *Brand {
	ch := make(chan *Brand, 10)

	go func() {
		findOptions := options.Find()
		findOptions.Projection = bson.M{
			"_id":  1,
			"name": 1,
			"data.plan.activePlan": 1,
			"data.allContacts.totalCount": 1,
		}

		query := bson.M {
			"lastLogin": bson.M { "$gt": now.AddDate(0, 0, -inactivityDays) },
			"data.plan.activePlan": "free",
		}

		collection := r.mongoClient.Database("shared").Collection("brands")
		cur, err := collection.Find(context.Background(), query, findOptions)
		if err != nil {
			log.Fatal(err)
		}

		for cur.Next(context.Background()) {
			elem := Brand{}
			err := cur.Decode(&elem)
			if err != nil {
				log.Fatal(err)
			}

			ch <- &elem
		}
		close(ch)
	}()

	return ch
}

func (r *brandsReader) ReadApiAccessibleBrandIds() <-chan *string {
	ch := make(chan *string, 10)

	go func() {
		findOptions := options.Find()
		findOptions.Projection = bson.M{
			"brandID":  1,
		}

		query := bson.M {}

		collection := r.mongoClient.Database("shared").Collection("apiKeys")
		cur, err := collection.Find(context.Background(), query, findOptions)
		if err != nil {
			log.Fatal(err)
		}

		for cur.Next(context.Background()) {
			var elem string
			err := cur.Decode(&elem)
			if err != nil {
				log.Fatal(err)
			}

			ch <- &elem
		}
		close(ch)
	}()

	return ch
}
